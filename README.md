# Sphinx

Ein Kurs und Nachschlagewerk für die Dokumentation im rst-Format.

Die aktuelle Build-Version ist direkt abrufbar unter:

https://oer-kurse.gitlab.io/sphinx/

Peter Koppatz (2022)


