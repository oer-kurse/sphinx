===============
Export (Docset)
===============

  
.. |a| image:: ./images/hi-giraffen.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Mann auf zwei Giraffen


Die Publikation im Netz ist heute Standard, hat aber einen kleinen
Nachteil! Die Suche über das Internet kann viele Klickorgien und ein
aufwendiges hin und her im Meer des Wissens initiieren.

Wie wäre es mit einer Offline-Suche?

Dieses Problem wird durch den Offline-Reader Dart gelöst, eine
Entwicklung von Bogdan Popescu Kapeli. Es existieren schon viele Docsets
und was wichtig ist, mit »Zeal« gibt es auch einen Offline-Reader für Windows
und Linux, der Docsets für Dart nutzen kann.


.. index:: export; DocSet

Für die Umwandlung eines Sphinx-Projektes benötigt man ein Paket, das
die Konvertierung automatisiert. Hynek Schlawack hat ein solches
entwickelt und es »doc2dash« genannt.
 
Anmerkung: Mit pip bin ich erstmalig gescheitert und bin der Empfehlung gefolgt,
pip durch pipx zu ersetzten. Damit werden Installation besser isoliert
und ein Nebeneinander unterschiedlichster Versionen ist möglich. 
	   
.. code:: bash 

   pip install pipx
   pipx install doc2dash


Ablauf zur Erstellung
=====================
Der EMACS ist kein muss, aber ganz praktisch...

.. image:: ./images/denote-workflow.svg
	    
   
Der Übersetzungsprozess
=======================
Das schon vorhandene »build«-Verzeichnis kann nun, statt einer direkten
Verwendung im Browser, mit einem zusätzlichen Schritt, für den Einsatz 
in den Offline-Readern »Dash«  bzw. »Zeal« aufbereitet werden.

.. code:: 

   doc2dash  -f build -i source/_static/emacs.png 

Wenn doc2dash nicht im Suchpfad zu finden ist, benötigt man den absoluten Pfad.
Nachfolgend einige Systemabhängige Standard-Pfad-Angaben:


.. tab:: MacOS

   .. code:: bash

      /Users/<usernam>/.local/bi/doc2dash

.. tab:: Linux

   .. code:: bash

      /home/<username>/.local/bin/doc2dash


.. tab::  Windows

   .. code:: bash

      C:/Users/<username>/.local/bin/doc2dash.exe
      
   
Kopieren wohin?
===============

Das ist abhängig vom verwendeten System, nachfolgend die
unterschiedlichen Ablageordner:


.. tab:: MacOS

   .. code:: bash

      /Users/<username>/Library/Application Supprot/Dash/DocSets      

.. tab:: Linux

   .. code:: bash
	     
      /home/<username>/.local/share/Zeal/Zeal/docsets


.. tab::  Windows

   .. code:: bash

      C:\\User\<username>\AppData\Local\Zeal\Zeal\docsets
      
	 

Einmal dort platziert, wird das neue Docset im Offline Reader
angeboten und steht für alle Nachschlageatkionen zur Verfügung.
Eventuell muss der Reader neu gestartet werden.

   
