==============
Export (lokal)
==============

Nicht immer ist HTML das finale Ausgabeformat, spielt aber die zentrale Rolle.
Die Unterstützung anderer Zielformate wird maximal unterstützt.

.. |a| image:: ./images/hi-giraffen.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Mann auf zwei Giraffen

Liste aller Standardoptionen
============================

.. index:: export-Optionen
	   
.. code:: text

   make build

   Sphinx v4.2.0
   Please use `make target' where target is one of

   html        to make standalone HTML files
   dirhtml     to make HTML files named index.html in directories
   singlehtml  to make a single large HTML file
   pickle      to make pickle files
   json        to make JSON files
   htmlhelp    to make HTML files and an HTML help project
   qthelp      to make HTML files and a qthelp project
   devhelp     to make HTML files and a Devhelp project
   epub        to make an epub
   latex       to make LaTeX files, you can set PAPER=a4 or PAPER=letter
   latexpdf    to make LaTeX and PDF files (default pdflatex)
   latexpdfja  to make LaTeX files and run them through platex/dvipdfmx
   text        to make text files
   man         to make manual pages
   texinfo     to make Texinfo files
   info        to make Texinfo files and run them through makeinfo
   gettext     to make PO message catalogs
   changes     to make an overview of all changed/added/deprecated items
   xml         to make Docutils-native XML files
   pseudoxml   to make pseudoxml-XML files for display purposes
   

Von Sphinx zum Buch?
====================

.. image:: ./images/jupyter-books.svg
   :width: 300px

Jupyter-books
-------------

Ausführbare Bücher, d.h. in einem Sphinx-Projekt werden Jupyter Notebooks eingebunden.


https://jupyterbook.org/intro.html

::

   pip install -U jupyter-book
   jupyter-book create meinbuch
   cd meinbuch
   jupyter-book build .

Mit Pandoc zu weiteren Zielformaten
-----------------------------------

Pandoc unterstützt viele Formate, falls die
Möglichkeiten mit Sphinx nicht zum Ziel führen.

::

   pandoc --list-output-formats

   asciidoc, beamer, commonmark, context, docbook, docbook4,
   docbook5, docx, dokuwiki, dzslides, epub, epub2, epub3, fb2,
   gfm, haddock, html, html4, html5, icml, jats, json, latex,
   man, markdown, markdown_github, markdown_mmd,
   markdown_phpextra, markdown_strict, mediawiki, ms, muse,
   native, odt, opendocument, opml, org, plain, pptx, revealjs,
   rst, rtf, s5, slideous, slidy, tei, texinfo, textile, zimwiki

 
