===============
Export (GitLab)
===============

  
.. |a| image:: ./images/hi-giraffen.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Mann auf zwei Giraffen


Für den Export im HTML-Format gibt es Services wie GitHub, GitLab und andere
Anbieter, die ein Repository nutzen um automatisch Prozesse anzustoßen.

Für Sphinx bedeutet das, es wird eine Virtuelle Maschine gestartet, die
Software installiert und zum Schluß das HTML erzeugt, wie es auf dem
eigenen Rechner ablaufen würde.

Die folgenden Bilder zeigen die wichtigsten Schritte für GitLab.

1. Es wird ein GitLab-Account benötigt.
2. Zum eigenen Code für das Sphinxprojekt kommt eine Datei: *.gitlab-ci.yml*
   Diese Datei kann aus einem Template im Benutzer-Bereich von GitLab
   erzeugt und bearbeitet werden.   
3. Jede Änderung an einer Stelle im Code, stößt den Build-Prozess
   erneut an.
4. Nach erfolgreicher Übersetzung, steht ein Link zur Verfügung, der
   auf das generierte HTML verweist.

   
.. index:: export; GitLab

Das Script für die automatische Übersetzung:
	   
.. code:: text
	  
   stages:
     - deploy

   pages:
     stage: deploy
     image: python:3.9-slim
     before_script:
       - apt-get update && apt-get install make --no-install-recommends -y
       - python -m pip install sphinx furo sphinx_inline_tabs
     script:
       - make html
     after_script:
       - mv ./build/html/ ./public/
     artifacts:
       paths:
       - public
     rules:
       - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCHqqd

Eine zweite Variante publiziert nur das HTML aus dem build-Ordner.
Damit wird der Übersetzunglauf auf dem Gitlab-Server überflüssig.

.. code:: text

   stages:
     - deploy
   
   pages:
     stage: deploy
     image: alpine:latest
     script:
       - mv ./build/ ./public/
     artifacts:
       paths:
         - public
     rules:
       - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

Die Wichtigsten Menüs für den Publikationsprozess.

Der Editor
==========

  .. image:: ./images/gitlab-editor.jpg

Der Übersetzungsprozess
=======================

  .. image:: ./images/gitlab-piplines.jpg

Der Verweis auf die publizierten Webdokumente
=============================================

  .. image:: ./images/gitlab-pages.jpg
     
Gitlab verwendet eine eingene Domain für die neu erstellten Inhalte:

z.B: https://oer-kurse.gitlab.io/sphinx

