.. https://github.com/reinout/reinout.vanrees.org/blob/master/rvo/weblog.py
   https://stackoverflow.com/questions/18146107/how-to-add-blog-style-tags-in-restructuredtext-with-sphinx
   https://ablog.readthedocs.io/en/latest/index.html

=======
Bloggen
=======

.. image:: ./images/hi-schreiben.svg
   :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hi-schreiben.svg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/hi-schreiben.svg">
   </a>

.. sidebar:: Serie: Altägypten

   | |b|
   | Schreiben
   
|a|


Jede Software hat Vor- und Nachteile. Was das Bloggen betrifft,
sind hier Lösungsvorschläge für »Sphinx« gelistet.

.. index:: Bloggen

.. toctree::
   :maxdepth: 1

   loesungen
   zeichensprache
   sortierung
   metadaten
   
   
