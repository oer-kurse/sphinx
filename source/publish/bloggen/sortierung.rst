==================
Ordnung (zeitlich)
==================



.. INDEX:: Tastenkombinationen; kbd (Rolle)
.. index:: Rolle; kbd (Tastenkombinationen)

.. image:: ./images/hi-anordnen.svg
   :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('/_images/hi-anordnen.svg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="/_images/hi-anordnen.svg">
   </a>


.. sidebar:: Serie: Altägypten

   | |b|
   | Anordnen
   
|a|

.. note::

   Als ich des Suchens müde war,
   erlernte ich das Finden.

   -- Friedrich Nietzsche

Die Liste einzelner Artikel läßt sich in drei Varianten anordnen:

1. explizit/manuell

2. in alphabetischer Ordnung (automatisch)

3. nach Datum (automatisch)

Reihenfolge: explizit
---------------------

Die Reihenfolge wird definiert, indem die Artikel in der
Reihenfolge gelistet werden, wie sie auch zur Anzeige kommen
sollen.



:: 

   .. toctree::
      :maxdepth: 1

      artikel/2022/artikel01
      artikel/2022/artikel05
      artikel/2022/artiekl07

Reihenfolge: alphabetisch
-------------------------

Setzt man den Parameter :glob: ein, kann über den \* auf alle
Artikel in einem Ordner zugegriffen werden. Das spart Tipparbeit.
So wie das Betriebssystem die Dateien sortiert, werden sie
automatisch in der Dokumentation angeordnet.


.. literalinclude:: inhalt-sortiert.txt
   :language: bash
   :lines: 1-4, 6-7
   :emphasize-lines: 4

Reihenfolge: Datum
------------------

Hier sollten die Dateien einem Namensschema folgen, das eine
umgekehrte Anordnung unterstützt z.B.

- 2022-01-12.rst

- 2022-02-12.rst

- 2022-03-12.rst

- ...

Mit dem Schlüsselwort :reversed: kann dann die normale Sortierung
umgekehrt werden und der aktuellste Artikel steht dann immer am
Anfang der Liste.


.. literalinclude:: inhalt-sortiert.txt
   :language: bash
   :emphasize-lines: 5
