==============
Zeichensprache
==============


.. https://picscut.com/de/


.. INDEX:: Tastenkombinationen; kbd (Rolle)
.. index:: Rolle; kbd (Tastenkombinationen)

.. image:: ./images/hi-herz.svg
   :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/hi-herz.svg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img hight="250px" src="../../_images/hi-herz.svg">
   </a>


.. sidebar:: 

   | Serie: Altägypten
   | |b|
   | Herz
   
|a|

.. note::

   Ein Bild sagt mehr als tausend Worte.

   -- Chinsesische Weisheit

.. |schmunzeln1| image:: ./images/smiley03.svg
   :scale: 100

.. |schmunzeln2| image:: ./images/smiley04.svg
   :scale: 100

.. |herz| image:: ./images/herz.svg
   :scale: 100

   
Kann ein Bild, so viele unterschiedliche Abstufungen erzeugen, wie es
die Sprache vermag?

- Sie lächelte ihn an.
- Ihre Mundwinkel wanderten unmerklich nach oben.
- Der Frieden beginnt mit einem Lächeln [Mutter Theresa].

In neuer Iconographie würden alle Liebesromane so oder ähnlich beginnen...


   Sie |schmunzeln1| und er |schmunzeln2|, beide |herz| + |herz| ...

Welche Version regt die Fantasie mehr an?
