============
 Build html
============


.. meta::

  :description lang=de: Sphinx-Autobuild für die Transformation zu HTML.
  :keywords: Sphinx, sphinx-autobuild, Dokumentation
  
Der Übersetzungprozess ist ein sich ständig wiederholdener
Zyklus. Warum den Prozess nicht halbautomatisch Wiederholen?

Zwei Lösungen bieten sich an.

Sphinx-Autobuild
================

::

   pip install sphinx-autobuild
   
   # Der Aufruf dann:

   sphinx-autobuild source build --port 1234

Watchdog
========

::

   pip install watchdog

   # Der Aufruf dann:
   
   watchmedo shell-command \
     --patterns="*.rst" \
     --ignore-pattern='build/*' \
     --recursive \
     --command='make html'

   
