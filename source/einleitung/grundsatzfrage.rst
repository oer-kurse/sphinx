================
 Grundsatzfrage
================


Kann man eine Software zum Bloggen verwenden,
die von Entwicklern für Entwickler gemacht wurde?

Auf diese Frage will ich hier eine Antwort
geben und die lautet: »Ja« 

Aussagen, die meine These stützen:

1. Der beabsichtigte Zweck des Markup ist die Umwandlung von
   reStructuredText-Dokumenten in nützliche, strukturierte
   Datenformate [#f1]_.

2. Sphinx ist ein Werkzeug, das die Erstellung intelligenter
   und schöner Dokumentation erleichtert. Es wurde von
   Georg Brandl geschrieben und steht unter der BSD-Lizenz [#f2]_.


.. [#f1] https://docutils.sourceforge.io/rst.html

.. [#f2] https://www.sphinx-doc.org/en/master/
