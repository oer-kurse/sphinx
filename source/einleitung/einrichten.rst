=====================
Ersteinrichtung/Setup
=====================

Für den Einsteiger sind folgende Schritte zu prüfen
und gegebenfalls durchzuführen (zur Orientierung):

.. image:: ./images/sphinx-install.svg
   :width: 50%
