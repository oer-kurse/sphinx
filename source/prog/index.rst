=====================
 Programmiersprachen
=====================

Das Projekt wurde ursprünglich für die Python-Dokumentation entwickelt,
um guter Software auch schöne Dokumentation zur Seite zu stellen.

	  
.. toctree::
   :maxdepth: 1

   sprachen
   

.. tip:: Ist Software nicht ordentlich dokumentiert, ist sie
         fehlerhaft!


Aus einem Interview
===================

Die Frage lautet: Warum ist eine Transformation (von Mainframes) schwierig (Hervorhebung von mir)?

  **Florian Holl:** Kunden scheuen sich meist davor, weil diese
  Transformation für sie ein Sprung ins Ungewisse ist, bei dem sie den
  Aufwand und die Risiken schwer einschätzen können. Oftmals handelt es
  sich bei Mainframe-Umgebungen um gewachsene Strukturen, die **nicht**
  immer **vollständig** dokumentiert sind.

  Ein Beispiel dafür sind **schlecht** oder **gar nicht beschriebene**
  Schnittstellen zwischen den Applikationen – sowohl innerhalb des Mainframes
  als auch nach außen.

  Quelle: https://www.heise.de/news/Fujitsu-Ein-offener-und-anpassungsfaehiger-Mainframe-Dino-stirbt-nicht-aus-6495833.html
