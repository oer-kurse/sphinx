======
Anhang
======


.. |a| image:: ./images/hi-man-with-oar.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Mann mit Ruder

.. toctree::
   :maxdepth: 1

   einrichten
   konfiguration
   besondere-dateien
   uebung
   
