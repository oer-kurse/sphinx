=================
Spezielle Dateien
=================

Welche Seiten fallen durch eine besondere Struktur/Funktion aus dem Rahmen?

- Das Glossar ist ein Wörterbuch für Fachchinesisch.

  :ref:`glossar`  (interne Referenz: Standard-Label)
  
- ToDo-Listen

  :ref:`Todo-Liste <gtd>` (selbstdefiniertes Label)

- Linkliste

  :ref:`Linkliste -- eine inidviduille Zusammenstellung <linkliste>` (selbstdefiniertes Label)

- Index

  :ref:`genindex`

  Der Index fasst alle Stichworte zusammen und ordnet diese in alphabetischer Reihenfolge.
  (interne Referenz: Standard-Label)
     
