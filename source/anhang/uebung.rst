============================
Rohdaten zum Experimentieren
============================

Der folgende Download enthält Rohdaten, mit denen erste Versuche im
geplanten Workshop gestartet werden können.  Natürlich ist der
Einstieg in ein selbst gewähltes Thema viel interessanter.

.. note::

   Der Themenschwerpunkt liegt auf: Krieg und Frieden (aus aktuellem Anlass)!

Download:  :download:`Rohmaterial zum Üben (zip-Datei) <./files/workshop-material.zip>`


