:orphan:
   
.. _downloadliste:
   
=========
Downloads
=========

:HINWEIS: Wenn der Download nicht startet, bitte den rechten Mausklick
	  versuchen und anschließend »öffnen mit...« oder eine ähnliche
	  Möglichkeit nutzen.
	  
========================= ===============
Zweck                     Link
========================= ===============
Kursmaterial              :download:`Rohmaterial zum Üben (zip/143 kB) <_static/downloads/workshop-material.zip>`
Dokumentation             :download:`Offline-Doku für »Zeal/Dash« (zip/9 MB) <_static/downloads/Sphinx.docset.zip>`
========================= ===============
