=========
 Mermaid
=========

.. meta::

   :description lang=de: Sphinx Erweiterung »Mermaid« für grafische Darstellungen 
   :keywords: Sphinx, Mermaid, Grafiken  


Es ist eine JavaScript-Lösung [#fn1]_, um Diagramme mit einer an Markdown
angelehtne Syntax zu erzeugen.

Beispiel
--------

.. mermaid::

   sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!


.. [#fn1] http://mermaid.js.org/intro/       
