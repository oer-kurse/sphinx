Sphinx mit dem ganzen ReST
==========================

.. meta::

   :description lang=de: Sphinx Kurs und Nachschlagewerk für die Dokumentation mit Python 
   :keywords: Sphinx, Dokumentation, Python Tutorial, OER, OER-Kurs 

.. sidebar:: Verzeichnisse

   | :ref:`genindex`
   | :ref:`linkliste`
   | :ref:`downloadliste`
   

Jede gute Software bietet Möglichkeiten der Dokumentation. In der
Python-Welt ist es die Software **Sphinx**.  Die Dateierweiterung
**rst** steht für »Resturctured Text« ist aber nicht generell darauf
festgelegt, sondern unterstützt auch die Dateiformate Text (txt)
bzw. Markdown (md).
	      
.. toctree::
   :maxdepth: 2
   :caption: Einleitung:

   einleitung/index
   inhalte/index
   prog/index
   publish/index
   templating/index
   extensions/index
   anhang/index
