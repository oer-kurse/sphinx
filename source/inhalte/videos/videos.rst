.. index:: Videos einbinden

====================
Videos (HTML -- raw)
====================

.. index:: Video

.. |a| image:: ./images/hi-watch.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   wachsam sein


Quelle
------
::

  | Blender: pyMove3D. 
  |

  .. raw:: html

     <iframe width="420" height="315" 
             src="//www.youtube.com/embed/URMOS4ryai0" 
  	     frameborder="0" allowfullscreen>
     </iframe>

  |
  `Video-Link: http://youtu.be/URMOS4ryai0 <http://youtu.be/URMOS4ryai0>`_


Ergebnis
--------

| Blender: pyMove3D 
|

.. raw:: html

   <iframe width='560' height='315' 
           src='//www.youtube.com/embed/URMOS4ryai0?feature=player_embedded'
           frameborder='0' allowfullscreen>
   </iframe>

|

`Video-Link: http://youtu.be/URMOS4ryai0 <http://youtu.be/URMOS4ryai0>`_

