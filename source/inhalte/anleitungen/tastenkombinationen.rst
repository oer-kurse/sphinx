===================
Tastenkombinationen
===================


.. INDEX:: Tastenkombinationen; kbd (Rolle)
.. index:: Rolle; kbd (Tastenkombinationen)

.. |a| image:: ./images/finger.svg

.. sidebar:: Serie: Altägypten

   |a|
   
   Finger

Obwohl ein weing aus der Mode gekommen, weil sich Hand und Maus
nur noch mit Gewalt trennen lassen, sind manche Arbeitschritte mit
den richtigen Tastenkombinationen schneller erledigt als durch
Mausgeschubse. Die ersten Tastenkombinationen für den EMACS sehen
wie folgt aus:

:kbd:`C-x`, :kbd:`C-s` Datei speichern

:kbd:`C-x`, :kbd:`C-c` Emacs schließen (Warum eigentlich?)

:kbd:`C-x`, :kbd:`C-f` Datei öffnen

:kbd:`C-x`, :kbd:`f` Spalte für Zeilenumbruch definieren (fill)

:kbd:`C-g`, :kbd:`c-g`, :kbd:`c-g` Abbruch der Aktion!

Windows-Beispiele
-----------------

Echt jetzt, auch Windows läßt sich mit Tastenkombinationen
bedienen:

   
:kbd:`Win-m` Alle Fenster verkleinern

:kbd:`C-a`, alles markieren

:kbd:`C-c`, Markiertes kopieren

:kbd:`C-x`, Markiertes ausschneiden

:kbd:`C-v` aus Zwischenablage einfügen

:kbd:`Alt-Tab` Umschalten zw. laufenden Anwendungen

Definition
----------

Es handelt sich um eine songenante Rolle, **:kbd:** ist eine davon.
Quellcode für den Beispielblock weiter oben.

   
.. code :: text 
   
   :kbd:`Win-m` Alle Fenster verkleinern

   :kbd:`C-a`, alles markieren

   :kbd:`C-c`, Markiertes kopieren

   :kbd:`C-x`, Markiertes ausschneiden

   :kbd:`C-v` aus Zwischenablage einfügen

   :kbd:`Alt-Tab` Umschalten zw. laufenden Anwendungen

Was bringt die Zukunft?
-----------------------

Vielleicht sind es in Zukunft eher Sprache oder
Gedanken, mit denen wir elektronischen Geräte steuern.
Ob es immer gelingen wird? Einige Zeitgenossen,
bekommen es jetzt schon nicht hin, die wichtigsten
Kommandos an ihren Liebling zu senden. z.B.

:kbd:`Sitz!`, :kbd:`Platz!`, :kbd:`Aus!`, :kbd:`Hier!`, :kbd:`Stopp` und :kbd:`Fuß!`  

:math:`\ddot\smile` 

Andere Quellen
--------------

- `https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html <https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html>`_

- `https://documentation.help/Sphinx/inline.html#role-kbd <https://documentation.help/Sphinx/inline.html#role-kbd>`_
