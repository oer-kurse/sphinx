.. meta::

   :description lang=de: Sphinx -- Python: Bilder einbinden
   :keywords: image, Bilder 

.. index:: Bilder einbinden

.. |a| image:: ./images/hi-falcon-in-boat.svg

================
Bilder einbinden
================


.. sidebar:: Serie: Altägypten

   |a|
   Falke im Boot

::

   .. image:: ./images/lyrische_gedichte_s160_unglueck.png
      :width: 600px

.. image:: ./images/lyrische_gedichte_s160_unglueck.png
   :width: 400px
