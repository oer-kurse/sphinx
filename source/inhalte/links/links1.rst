.. meta::

   :description lang=de: Sphinx -- Python: Verlinkung
   :keywords: Sphinx, Python, Links 


.. index:: Verlinkungen, Links, ref

.. _link1:

=================
 Links (1 von 2)
=================

.. index:: Verlinkung 

.. |a| image:: ./images/hi-sledge-01.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Schlitten


Verlinkung zu einer externen Website
====================================

Quelltext
---------
::

  # Definition (vielleicht am Textende)

  .. _Website Python: https://python.org
      -------+------  ---------+--------
             |                 |
	     |                 o---- Link (nicht direkt sichtbar)
             o---------------------- Linkbeschriftung (sichtbar)
	     
  # Anwendung (im Text)

  `Website Python`_
  

Ergebnis
--------

`Website Python`_


Verlinkung zu einer anderen Seite (intern)
==========================================

Referenzen, damit können Querverweise im Dokument erzeugt werden.

1. einen Zielnamen definieren (siehe andere Seite)

   Quelltext:

   ::
   
      .. _links2:
   
2. Zum Ziel verweisen...  
  
   ::

      :ref:`Sprung zu einer anderen Seite mit dem Sprungziel *link2* <link2>`

.. hint:: **Kein** Leerzeichen zwischen :ref: und dem ersten Akzent (`)!
   
	   
Ergebnis
---------

:ref:`Sprung zu einer anderen Seite mit dem Sprungziel *link2* <link2>`

.. index:: download

Download-Link
=============
Quelltext:

::

  Download: :download:`UML-Diagramme im SVG-Format (zip-Datei) <files/bilder-im-svg-format.zip>`

Ergebnis:

Download:  :download:`UML-Diagramme im SVG-Format (zip-Datei) <files/bilder-im-svg-format.zip>`

Link-Referenz
=============
Links sind oft sehr lang und können zahlreich im Text verteilt sein.
Eine Möglichkeit der »zentralen Verwaltung« am Ende der Datei
verbessert die Lesbarkeit im Text und die Übersicht.

1. Sammeln sie alle Links am Ende eines Dokumentes.

   ::

     .. _PDF zum Lehrgang in Hamburg:   /static/download/hamburg/hamburg2014.pdf
     .. _PDF zum Lehrgang in Stuttgart: /static/download/stuttgart/stuttgart2015.pdf

2. Im Text verweisen Sie dann auf diese Links:

   ::

   `PDF zum Lehrgang in Stuttgart`_
   `PDF zum Lehrgang in Hamburg`_


Hat sich der Dateiname geändert, muss er nur an einer Stelle (am Textende)  ausgetauscht werden.
Der Verweis im Text ist davon nicht betroffen.
   
.. _Website Python: https://python.org
