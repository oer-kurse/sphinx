.. index:: Verlinkungen, Links

.. _link2:

=================
 Links (2 von 2)
=================

.. index:: Verlinkung 

.. |a| image:: ./images/hi-sledge-02.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Schlitten mit Kopf
   eines Schakal


:ref:`Und wieder zurück...<link1>`


Rücksprung zur vorherigen Seite
===============================

Mit einer Referenzen, erzeugt man den Rücksprung ersten Seite, von der wir hierher gelangt sind...

:ref:`Sprung zur Seite mit dem Label: 'link1' <link1>`

::

   :ref:`Sprung zur Seite mit dem Label: 'link1' <link1>`
